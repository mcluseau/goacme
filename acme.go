package acme

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"errors"
	"flag"
	"fmt"
	"log"
	"math/big"
	"net"
	"strings"
	"sync"
	"time"

	"github.com/go-acme/lego/v3/certcrypto"
	"github.com/go-acme/lego/v3/certificate"
	"github.com/go-acme/lego/v3/challenge/tlsalpn01"
	"github.com/go-acme/lego/v3/lego"
	"github.com/go-acme/lego/v3/registration"

	lru "github.com/hashicorp/golang-lru"
	bolt "go.etcd.io/bbolt"
)

// FIXME lots of cleanup to do here

var (
	AllHosts []string
	MyIPs    []string
	Notify   func(host string, changed bool, key, crt []byte)

	acmeURL         = flag.String("acme-url", "https://acme-staging-v02.api.letsencrypt.org/directory", "ACME provider URL")
	acmeEmail       = flag.String("acme-email", "", "ACME email")
	acmeStorePath   = flag.String("acme-store", "acme.db", "ACME store")
	acmeCheckPeriod = flag.Duration("acme-check-period", 10*time.Minute, "period of ACME checks")
	acmeCrtMaxAge   = flag.Int("acme-cert-max-age", 60, "maximum age of an ACME certificate before renewing")

	acmeStore *bolt.DB
	globalBkt = []byte("global")
	hostsBkt  = []byte("hosts")

	legoUser acmeUser

	// store keys
	key_pk           = []byte("private-key")
	key_registration = []byte("registration")

	defaultCert *tls.Certificate

	ErrDisabled = errors.New("no ACME e-mail, support is disabled")
)

type acmeUser struct {
	Email        string
	Registration *registration.Resource
	key          crypto.PrivateKey
}

var user acmeUser

func (u *acmeUser) GetEmail() string {
	return u.Email
}
func (u *acmeUser) GetRegistration() *registration.Resource {
	return u.Registration
}
func (u *acmeUser) GetPrivateKey() crypto.PrivateKey {
	return u.key
}

func Open() (err error) {
	if *acmeEmail == "" {
		return ErrDisabled
	}

	store, err := bolt.Open(*acmeStorePath, 0600, nil)
	if err != nil {
		return fmt.Errorf("failed to open ACME store: %v", err)
	}

	user = acmeUser{Email: *acmeEmail}

	err = store.Update(func(tx *bolt.Tx) (err error) {
		b, err := tx.CreateBucketIfNotExists(globalBkt)
		if err != nil {
			return
		}

		_, err = tx.CreateBucketIfNotExists(hostsBkt)
		if err != nil {
			return
		}

		// get or create default certificate
		defaultCertBytes, err := bktGetOrCreate(b, "default-certificate", genDefaultCert)
		if err != nil {
			return
		}

		cert, err := tls.X509KeyPair(defaultCertBytes, defaultCertBytes)
		if err != nil {
			return
		}

		defaultCert = &cert

		// get or create private key
		pkBytes, err := bktGetOrCreate(b, "private-key", func() (ba []byte, err error) {
			log.Print("ACME: generating new key")
			key, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
			if err != nil {
				return
			}

			return x509.MarshalECPrivateKey(key)
		})

		if err != nil {
			return
		}

		user.key, err = x509.ParseECPrivateKey(pkBytes)
		return
	})

	if err != nil {
		return fmt.Errorf("ACME: failed to initialize: %v", err)
	}

	store.Sync()

	legoUser = user
	acmeStore = store
	return
}

func setupLego() (client *lego.Client, err error) {
	// fetch registration info
	var regBytes []byte

	err = acmeStore.View(func(tx *bolt.Tx) (err error) {
		regBytes = tx.Bucket(globalBkt).Get([]byte("registration"))
		return
	})

	if err != nil {
		log.Print("ACME: failed to read registration: ", err)
		return
	}

	registered := len(regBytes) != 0

	if registered {
		user.Registration = &registration.Resource{}
		err = json.Unmarshal(regBytes, user.Registration)
		if err != nil {
			log.Print("ACME: failed to parse registration, ignoring.")
			regBytes = nil
		}
	}

	user := legoUser

	// setup LEGO
	config := lego.NewConfig(&user)
	config.CADirURL = *acmeURL
	config.Certificate.KeyType = certcrypto.RSA4096

	client, err = lego.NewClient(config)
	if err != nil {
		log.Fatal("ACME: failed to create client: ", err)
	}

	err = client.Challenge.SetTLSALPN01Provider(Provider)
	if err != nil {
		log.Fatal("ACME: failed to set TLS-ALPN-01 provider: ", err)
	}

	if !registered {
		// no registration
		log.Print("ACME: registering...")

		user.Registration, err = client.Registration.Register(registration.RegisterOptions{
			TermsOfServiceAgreed: true,
		})

		if err != nil {
			log.Print("ACME: failed to register account: ", err)
			return
		}

		regBytes, err = json.Marshal(user.Registration)
		if err != nil {
			log.Print("ACME: failed to encode registration: ", err)
			return
		}

		acmeStore.Batch(func(tx *bolt.Tx) error {
			return tx.Bucket(globalBkt).Put([]byte("registration"), regBytes)
		})

	} else {
		user.Registration, err = client.Registration.ResolveAccountByKey()
		if err != nil {
			log.Print("ACME: failed to resolve account: ", err)
			return
		}
	}

	return
}

func bktGetOrCreate(b *bolt.Bucket, key string, create func() ([]byte, error)) (ba []byte, err error) {
	keyB := []byte(key)

	ba = b.Get(keyB)

	if ba == nil {
		ba, err = create()
		if err != nil {
			return
		}

		err = b.Put(keyB, ba)
	}

	return
}

func genDefaultCert() (certBytes []byte, err error) {
	log.Print("ACME: generating default certificate")

	pk, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return
	}

	const defaultHostName = "default-certificate"

	now := time.Now()

	certTemplate := x509.Certificate{
		SerialNumber: big.NewInt(now.Unix()),
		Subject: pkix.Name{
			CommonName: defaultHostName,
		},
		NotBefore: now,
		NotAfter:  now.Add(5 * 24 * 365 * time.Hour),

		IsCA:        true,
		KeyUsage:    x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},

		BasicConstraintsValid: true,

		DNSNames: []string{defaultHostName},
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &certTemplate, &certTemplate, pk.Public(), pk)
	if err != nil {
		return
	}

	pkBytes, err := x509.MarshalECPrivateKey(pk)
	if err != nil {
		return
	}

	// create PEM encoded data
	buf := &bytes.Buffer{}

	err = pem.Encode(buf, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	if err != nil {
		return
	}

	err = pem.Encode(buf, &pem.Block{Type: "PRIVATE KEY", Bytes: pkBytes})
	if err != nil {
		return
	}

	return buf.Bytes(), nil
}

var acmeCheckTrigger = make(chan bool, 100)

func RunWorker() {
	ticker := time.Tick(*acmeCheckPeriod)

	for {
		select {
		case <-acmeCheckTrigger:
		case <-ticker:
		}

		Check()
	}
}

var checkMutex = sync.Mutex{}

func Check() {
	checkMutex.Lock()
	defer checkMutex.Unlock()

	allHosts := AllHosts

	if acmeStore == nil {
		return
	}

	log.Print("ACME: performing checks")

	missingHosts := make([]string, 0)

	notify := Notify // stable Notify for this execution
	notifs := make([]func(), 0)

	acmeStore.View(func(tx *bolt.Tx) (err error) {
		b := tx.Bucket(hostsBkt)

		for _, name := range allHosts {
			name := name

			keyBytes := b.Get([]byte(name + "/key"))
			if keyBytes == nil {
				log.Print("ACME: - no key for ", name)
				missingHosts = append(missingHosts, name)
				continue
			}

			certsBytes := b.Get([]byte(name + "/crt"))
			if certsBytes == nil {
				log.Print("ACME: - no certificate for ", name)
				missingHosts = append(missingHosts, name)
				continue
			}

			if notify != nil {
				notifs = append(notifs, func() { notify(name, false, keyBytes, certsBytes) })
			}

			// got a certificate, check for expiration
			block, _ := pem.Decode(certsBytes)
			if block == nil || block.Type != "CERTIFICATE" {
				log.Print("ACME: - certificate for ", name, " is invalid.")
				missingHosts = append(missingHosts, name)
				continue
			}

			crt, err := x509.ParseCertificate(block.Bytes)
			if err != nil {
				log.Print("ACME: - certificate for ", name, " is invalid: ", err)
				missingHosts = append(missingHosts, name)
				continue
			}

			if age := int(time.Since(crt.NotBefore) / (24 * time.Hour)); age >= *acmeCrtMaxAge {
				log.Printf("ACME: - certificate for %s needs to be refreshed (%d days old)", name, age)
				missingHosts = append(missingHosts, name)
				continue
			}
		}

		return
	})

	for _, notif := range notifs {
		notif()
	}

	if len(missingHosts) == 0 {
		return
	}

	var legoClient *lego.Client

	for _, host := range missingHosts {
		log.Print("ACME: fetching certificate for ", host)

		addrs, err := net.LookupHost(host)
		if err != nil {
			log.Print("ACME: - failed to resolve ", host, ": ", err)
			continue
		}

		hasGoodAddress := false

	addrLoop:
		for _, addr := range addrs {
			for _, myAddr := range MyIPs {
				if myAddr == addr {
					hasGoodAddress = true
					break addrLoop
				}
			}
		}

		if !hasGoodAddress {
			log.Print("ACME: - host does not resolve to any of my IPs: ", addrs)
			continue
		}

		request := certificate.ObtainRequest{
			Domains: []string{host},
			Bundle:  true,
		}

		if legoClient == nil { // lazy init of lego client
			legoClient, err = setupLego()
			if err != nil {
				return
			}
		}

		certs, err := legoClient.Certificate.Obtain(request)
		if err != nil {
			log.Print("- failed to obtain certificates: ", err)
			continue
		}

		certsBytes, err := json.Marshal(certs)
		if err != nil {
			return
		}

		crt := make([]byte, 0, len(certs.Certificate)+len(certs.IssuerCertificate))
		crt = append(crt, certs.Certificate...)
		crt = append(crt, certs.IssuerCertificate...)

		err = acmeStore.Batch(func(tx *bolt.Tx) (err error) {
			for _, kv := range []struct {
				k string
				v []byte
			}{
				{host, certsBytes},
				{host + "/key", certs.PrivateKey},
				{host + "/crt", crt},
			} {
				err = tx.Bucket(hostsBkt).Put([]byte(kv.k), kv.v)
				if err != nil {
					return
				}
			}

			return
		})

		if err != nil {
			log.Print("- failed to record certificates: ", err)
			continue
		}

		if notify := Notify; notify != nil {
			notify(host, true, certs.PrivateKey, crt)
		}

		certCache.Remove(host)
	}
}

var certCache, _ = lru.New(1024)

func SetupConfig(cfg *tls.Config) {
	cfg.NextProtos = []string{tlsalpn01.ACMETLS1Protocol}
	cfg.GetCertificate = GetCertificate
}

func GetCertificate(hello *tls.ClientHelloInfo) (cert *tls.Certificate, err error) {
	if hello.ServerName == "" {
		cert = defaultCert
		return
	}

	serverName := strings.ToLower(hello.ServerName)

	// check ACME certs
	for _, proto := range hello.SupportedProtos {
		if proto == tlsalpn01.ACMETLS1Protocol {
			if i, ok := Provider.lru.Get(serverName); ok {
				log.Print("ACME request: serving challenge for ", serverName)
				cert = i.(*tls.Certificate)
				return
			}
			break
		}
	}

	cert, err = GetCertificateForHost(serverName)

	if err != nil {
		return
	}

	if cert == nil {
		cert = defaultCert
	}

	return
}

func GetCertificateForHost(serverName string) (cert *tls.Certificate, err error) {
	// check cache
	if i, ok := certCache.Get(serverName); ok {
		cert = i.(*tls.Certificate)
		return
	}

	if acmeStore == nil {
		return
	}

	// load from DB
	err = acmeStore.View(func(tx *bolt.Tx) (err error) {
		key := tx.Bucket(hostsBkt).Get([]byte(serverName + "/key"))
		crt := tx.Bucket(hostsBkt).Get([]byte(serverName + "/crt"))

		if key == nil || crt == nil {
			return
		}

		dbCert, err := tls.X509KeyPair(crt, key)
		if err != nil {
			return
		}

		cert = &dbCert
		return
	})

	if err != nil {
		return
	}

	// add to cache
	certCache.Add(serverName, cert)

	return
}
