package acme

import (
	"github.com/go-acme/lego/v3/challenge/tlsalpn01"
	lru "github.com/hashicorp/golang-lru"
)

var (
	Provider tlsalpn01Provider
)

func init() {
	lruStore, _ := lru.New(128)
	Provider = tlsalpn01Provider{lru: lruStore}
}

type tlsalpn01Provider struct {
	lru *lru.Cache
}

func (p tlsalpn01Provider) Present(domain, token, keyAuth string) (err error) {
	cert, err := tlsalpn01.ChallengeCert(domain, keyAuth)
	if err != nil {
		return
	}

	p.lru.Add(domain, cert)

	return
}

func (p tlsalpn01Provider) CleanUp(domain, token, keyAuth string) error {
	p.lru.Remove(domain)
	return nil
}
