module m.cluseau.fr/goacme

go 1.14

require (
	github.com/cenkalti/backoff/v4 v4.1.0 // indirect
	github.com/go-acme/lego/v3 v3.9.0
	github.com/hashicorp/golang-lru v0.5.4
	github.com/miekg/dns v1.1.35 // indirect
	gitlab.com/mcluseau/goacme v0.0.0-20200918125500-8fc21aa0b054 // indirect
	go.etcd.io/bbolt v1.3.5
	golang.org/x/crypto v0.0.0-20201208171446-5f87f3452ae9 // indirect
	golang.org/x/net v0.0.0-20201209123823-ac852fbbde11 // indirect
	golang.org/x/sys v0.0.0-20201214095126-aec9a390925b // indirect
	golang.org/x/text v0.3.4 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
